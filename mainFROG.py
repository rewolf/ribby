from numpy import size, mean, log, pi, zeros, max, sum, round, diff, trapz, flipud, arange, exp, angle, roll, sqrt
from numpy.fft import fft, fftshift
from numpy.random import normal, uniform, randint
import numpy as np
from makeFROG import makeFROG
from makePulse import makePulse
from analyze import analyzepulse, plotFROG
import scipy.optimize as opt
import matplotlib.pyplot as ppl

iterationVector = []
errorVector = []

def rmsdiffalpha(alpha,coeffs):
    return sqrt(mean((coeffs[0]-alpha*coeffs[1])**2))

def mainFROG(originalFROG, errorTolerance, maxIterations, deltaDelay, whichMethod, retrievedPulse):
    errorVector = []
    
    
    # RMS difference in the entries of two real matrices/vectors
    rmsdiff = lambda f1, f2: sqrt(mean((f1-f2)**2))



    # get trace dimensions
    N = size(originalFROG, 1)

    # normalize FROG trace to unity max intensity
    # VK: this is suggested in DeLong1996 VII.
    originalFROG = originalFROG/max(originalFROG)
    
    if (retrievedPulse is None): 
        # generate initial guess
        initialIntensity = exp(-2*log(2)*((arange(0,N).T-N/2)/(N/10))**2)
        initialPhase = 1 #no chirp
        retrievedPulse = initialIntensity*initialPhase

    (retrievedFROG, retrievedEFROG) = makeFROG(retrievedPulse)

#   ------------------------------------------------------------
#   F R O G   I T E R A T I O N   A L G O R I T H M
#   ------------------------------------------------------------

    Niteration = 1
    GError = 1e10
    bestErrorRetrieved = GError,retrievedPulse

    while ((GError > errorTolerance) and (Niteration < maxIterations)):

        #VK: This is formula (3) of DelongGP1994.
        #Or it comes from TrebinoReview1997:
        #In order to perform a GP to the FROG-trace data constraint set, 
        #it is simply necessary to replace the magnitude of EFROG with the square 
        #root of the measured FROG trace.
        #This looks to be common for all algorithms and know also as "intensitiy constraint".
        #retrievedEFROG = retrievedEFROG*(sqrt(originalFROG/retrievedFROG))
        retrievedEFROG = retrievedEFROG*(sqrt(np.divide(originalFROG, retrievedFROG, out=np.zeros_like(originalFROG), where=retrievedFROG!=0)))

        #Extract pulse field from FROG complex amplitude
        #This can be Vanilla/Basic,GP or PCGP_SVD or PCGP_Power variations
        retrievedPulse = makePulse(retrievedEFROG, retrievedPulse, whichMethod)

        # use weighted average to keep peak centered at zero
        centerIndex = sum((arange(0, N)).T*abs(retrievedPulse**4))/sum(abs(retrievedPulse**4))
        retrievedPulse = roll(retrievedPulse, int(-round(centerIndex-N/2)))
        
        # make a FROG trace from new fields
        (retrievedFROG, retrievedEFROG) = makeFROG(retrievedPulse)

        # calculate FROG error G, scale Fr to best match Fm, see DeLong1996,
        # and femtosoft error - intensity weighted
        #finalGError =rmsdiff(originalFROG,retrievedFROG)
        alpha0=1
        mycoeffs=[originalFROG,retrievedFROG]
        results = opt.minimize(rmsdiffalpha,alpha0,args=mycoeffs)
        GError=results.fun
        #retrievedEFROG*=results.x #normalise FROG E matrix to found alpha
        #print("alpha: ", results.x)

        # keeping track of error
        errorVector.append(GError)

        if (Niteration % 100 == 0):
            print(f'Iteration number: {Niteration} Error: {GError}')

        Niteration = Niteration + 1
        
        if (bestErrorRetrieved[0]>GError):
            bestErrorRetrieved=GError,retrievedPulse

    #CheckbestErrorRetrieved if the last is the best
    if (bestErrorRetrieved[0]<GError):
        print("current error value:",GError,", taking back the best: ", bestErrorRetrieved[0])
        GError=bestErrorRetrieved[0]
        retrievedPulse=bestErrorRetrieved[1]
    print(f'Iteration number: {Niteration} Error: {GError}')
    return (retrievedPulse, retrievedFROG, GError,  Niteration)      