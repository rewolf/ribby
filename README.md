# Ribby - Software for SHD FROG traces reconstruction
Authors: C. Hugon (R&DOM Marseille), V. Kulikovskiy (INFN Sezione di Genova).

## Main FROG reconstruction procedure (`mainFROG` function from `mainFROG.py`)
It has pulse creation starting from a Gauss initial pulse guess and then, iteratively:
- "Electric" (non-intensity) FROG (EFROG) normalisation to original FROG trace, known as "intensitiy constraint" [1-2].
- pulse construction from EFROG and current reconstruction pulse with following methods:
     - "Vanilla" - the original FROG inversion algorithm integrated the time domain FROG trace, with 
respect to the time delay, to obtain subsequent guesses for (so-called “vanilla” or “basic” algorithm).
The integration effectively reduces the gate function to a constant, yielding where is the integration 
constant and is the next guess for the electric field. While fast, this algorithm stagnates easily and 
fails to invert spectrograms of double pulses [4].
     - PGP SVD and PGP Power are Principal Component Generalized Projects methods described in [4].
- retrievedFROG creation from the pulse.
- Reconstruction FROG estimation as RMS of the bin by bin differences between.
originalFROG and alpha x retrievedFROG (to account for normalisation uncertainty) [3].

There is a mechanism to add random fluctuations to FROG trace if the reconstruction is stalled (simulated anneealing approach).
Randomization is controlled by temperature-like parameter which starts from 50% randomisation.


## Draw results (`DrawResults` from `mainFROG.py`)
- Draw FROG original image, reconstructed image (`plotFROG` from `draw.py`).
- Draw reconstructed pulse time profile and phase, spectral profile, phase (`plotpulse` from `draw.py`).
    - Phases are "unwrapped" with `unwrapphase` function.
    - Calculation of the chirp from spectral phase as a second derivative of phi(omega).
which is equal to GDD in case of unchirped pulse passage throght the dispersive medium [5].

## Read APE FROG raw data (`readrawAPE.py`)
Header three numbers are interpreded as frequency start (max) [Thz], frequency step[THz], time step [fs].
Each data line is an autocorellator scan at a fixed frequency.
Frequency is considered to be in the center of the bins of the grid 
from frequency start to frequency start + number of lines per frequency step.

## Tests notebook subfolder

- `FullAnalysis.ipynb` shows analysis of one APE FROG data (stored in `data` folder).
- `testGaussChirp.ipynb` tests of a syntetic Gauss pulse creation witha chirp and chirp characterisation.
- `testFROGGaussChirp.ipynb` tests of the chirp reconstruction from a syntetic Gauss pulse.      
- `testMakeFROG.ipynb` test for FROG trace creation from a pulse and check that SHG FROG trace is insensible to
phase shift and chirp sign.


## Known TODOs

- Cross-check convergence speed respect to the published results on implemented methods and optimise anneealing approach.
- Optimize initial pulse guess (gaussian fit of the progection and the pulse with corrected witdht?).
- Optimize FROG trace preparation from the data: 
    - step size choice and range should follow sugestion that FROG trace should look round.
    - fit with Gauss both projections and guess step sized to make the equal.
- Pulse analysis - currently bandwidth calculation and pulse duration is calculated as number of poinsts aboe 1/2 per step of the time/frequency.
Estimation from spline fit should be implemented.

# Licence 
The softaware is published under GPLv2 licence.

Ribby is a frog name from the Cuphead game.

# References
[1] K. DeLong "Pulse retrieval in frequency-resolved optical gating based on the method of generalized projections", OPTICS LETTERS / Vol. 19, No. 24 / December 15, 1994

[2] R. Trebino et al, "Measuring ultrashort laser pulses in the time-frequency domain using frequency-resolved optical gating", Rev. Sci. Instrum. 68 (9), September 1997

[3] K. DeLong and R. Trebino "Practical Issues in Ultrashort-Laser-Pulse Measurement Using Frequency-Resolved Optical Gating", IEEE JOURNAL OF QUANTUM ELECTRONICS, VOL. 32, NO. 7, JULY 1996

[4] D. Kane, "Recent Progress Toward Real-Time Measurement of Ultrashort Laser Pulses, IEEE JOURNAL OF QUANTUM ELECTRONICS, VOL. 35, NO. 4, APRIL 1999

[5] M. Wollenhaupt et al, "Femtosecond Laser Pulses: Linear Properties, Manipulation, Generation and Measurement", ISBN 978-0-387-95579-7
