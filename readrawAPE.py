import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import itertools
from scipy.interpolate import griddata

#See testAPEaxis.ipynb
#During APE FROG aquisition window we've seen that lambdamin=720,lambdamax=880,steps=200.
#The FROG maximum looked to be at 805nm where laser peak frequency is expected.
#FROG WL is half of this, so APE automatically rescales axis labels on FROG!
#In the APE raw data those numbers were written to the header 416.378414 0.378526 and 200 lines are written
#If they are interpreted as wlmax and wlstep then one gets lambdamin=720 applying factor of 2.
#one also gets lambdamax=880 if nbins and not nbins-1 are used.
#So  it looks like APE threats their data as a histogram (and not aquistion with precise frequencies)
#So, one should assume that each data has a frequency in the center of the bin.


def readrawAPE(filename,deltaT=10,nbin=64,debug=True):
    metadata=open(filename).readline().rstrip().split()
    data = np.loadtxt(filename,skiprows=1).T #Transposed for easier unpacking
    timestep = float(metadata[2])
    if debug: print("Step time:  ",timestep)
    #raw data grid definition
    trawgrid = np.arange(-data.shape[0]/2*timestep,(data.shape[0]/2)*timestep,timestep) #does array from -N/2DeltaT to (N/2+1)DeltaT following Kane1999 (but we don't know how APE scan is working ie if these range of delays is used 
    if debug: 
        print("Time steps:", data.shape[0],trawgrid.shape[0])
        print("Check if bin N/2 is 0:", trawgrid[int(trawgrid.shape[0]/2)])
 
    c_light = 2.99792458e8
 
    freqstep = float(metadata[1])*2  #FROG frequency is twice actually, but APE plots it as expected wl of the laser
    freqmax  = float(metadata[0])*2 - freqstep/2 #since the bin center of the first bin is half step shifted
    freqmin  = freqmax-freqstep*(data.shape[1]-1)
    frawgrid = np.linspace(freqmax,freqmin,data.shape[1])
    wrawgrid = c_light/frawgrid*1e-3*2 #1/nm/THz, factor 2 is added by APE to have pulse like wl
    
    if trawgrid.shape[0] != data.shape[0] or frawgrid.shape[0] != data.shape[1]:
        print("ERROR: shapes are wrong!")
  
    rawgrid = np.array(list(itertools.product(trawgrid, frawgrid)))
    
    if debug:
        plt.imshow(data.T,origin="lower",extent=(trawgrid[0], trawgrid[-1], wrawgrid[0], wrawgrid[-1]),aspect="auto")
        plt.title('Raw image')
        plt.xlabel('Delay [fs]')
        plt.ylabel('Wavelenght [nm]')
        plt.show()
    
#substraction background estimated on 5% left + 5% right of the time scale (autocorellator scan at fixed frequency)
    background = []
    for biny in range(0,data.shape[1]):
        bgarr = data[np.r_[0:int(0.05*data.shape[0]),int(0.95*data.shape[0]):data.shape[0]],biny]
        threshold = np.average(bgarr) 
        background.append((threshold,np.std(bgarr)))
        a = np.array([x - threshold for x in data[:,biny]])
        data[:,biny] = a
        
    if debug:
        plt.imshow(data.T,origin="lower",extent=(trawgrid[0], trawgrid[-1], wrawgrid[0], wrawgrid[-1]),aspect="auto")
        plt.title('After noise substraction')
        plt.xlabel('Delay [fs]')
        plt.ylabel('Wavelenght [nm]')
        plt.show()      
    
    #new data grid definition - centred at maximum of the FROG
    maxbin = np.where(data == np.amax(data))
    print(maxbin)
    maxbin = int(np.mean(maxbin[1])) #to deal with several max intesity FROG trace bins
    fcenter = frawgrid[maxbin]
    #time grid keeps 0 in the middle
    tnewgrid = np.linspace(-nbin/2*deltaT,(nbin/2-1)*deltaT,nbin)
    if debug: 
        print("maximum bin in data raw",maxbin,fcenter)
        print("new time grid:", tnewgrid[0], tnewgrid[-1])
        print("check nbin/2 time: ", tnewgrid[int(nbin/2)])
    
    deltaF = 1/(nbin*deltaT)*1000 #in THz
    fnewgrid = 1000*np.fft.fftshift(np.fft.fftfreq(nbin, deltaT))+fcenter
    
    newgrid1, newgrid2  = np.meshgrid(tnewgrid,fnewgrid)
    
    grid_result = griddata(rawgrid, data.flatten(), (newgrid1, newgrid2), method='cubic')
    a=np.reshape(grid_result, (nbin,nbin))

#remove negative values of intensity due to cubic spline interpolation
    a = np.nan_to_num(a) #set nan values to 0
    np.clip(a, a_min=0, a_max=1e10, out=a)
    
    #normalize to 1 at maximum
    a = a/np.max(a)
    background = background/np.max(a)
    
    #center time grid at 0 for pulse plotting later
    #tnewgrid = np.linspace(-nbin/2*deltaT,(nbin/2-1)*deltaT,nbin)
    #frequency grid has center at half frequency since FROG has twice carrier frequency
    #fnewgrid = 1000*np.fft.fftshift(np.fft.fftfreq(nbin, deltaT))+fcenter/2.
    if debug:
        print("Raw center half frequency: ",(frawgrid.max()+frawgrid.min())/4.,"THz")
        print("Expected max frequency:    ", fcenter/2., "THz")
    
    return a,fcenter,background
